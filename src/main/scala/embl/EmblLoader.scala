package embl

import java.io.{BufferedWriter, File, FileWriter}
import java.nio.file.{Files, Paths}
import scala.io.Source
import sys.process._
import scalaj.http._

import java.nio.file.StandardCopyOption.ATOMIC_MOVE

//possibly use trait
class EmblLoader(
                  input: String = "",
                  pathToMissingEntries: String = "",
                  output: String = "",
                  oneProc: Boolean = false
                ) {

  val baseUrl = "https://www.ebi.ac.uk/ena/browser/api/embl"
  val lineLimit = "lineLimit=1000"

  //create subdirectory
  def createSubDir(path:String,id:String) = {
    val filepath = Paths.get(path,id).toString
    s"mkdir -p $filepath".!
  }

  def createDir(path:String) = {
    val filepath = Paths.get(path).toString
    s"mkdir -p $filepath".!
  }

  def writeFile(filename:String, content: String) = {
      val file = new File(filename)
      val bw = new BufferedWriter(new FileWriter(file))
      bw.write(content)
      bw.close()
    }

  def moveAtomically(path: String, targetPath: String) = {
    Files.move(Paths.get(path),Paths.get(targetPath),ATOMIC_MOVE)
  }


  def getEmblData(id: String, output: String, tmp: String) = {
    val url = s"$baseUrl/$id?$lineLimit"
    val full_path = s"$output/embl_$id.embl"
    val tmp_path = s"$tmp/embl_$id.embl"
    if (! new File(full_path).exists()) //if exists, skip
    {
      val resp: HttpResponse[String] = Http(url).asString
      if (resp.is2xx) {
        writeFile(tmp_path,resp.body)
        moveAtomically(tmp_path,full_path)
      }
    }

  }

  def pullFiles(input: String, output: String, proc_flag: Boolean, tmp: String) = {

  }

  def getRecordIds(path:String):Seq[String] = {
    val source = Source.fromFile(path)
    val data = source.getLines()
    val ids = data.map{s =>
      s.split("\t").head.trim
    }
    source.close()
    ids.toSeq.tail
  }



}

object EmblLoader {
  def apply(input:String = "",
            pathToMissingEntries: String = "",
            output: String = "",
            oneProc: Boolean = false
           ) =
    new EmblLoader(input,pathToMissingEntries,output,oneProc)

}